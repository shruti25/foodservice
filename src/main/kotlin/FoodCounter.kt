import Status.Served
import Status.Start

class FoodCounter() {
    //val placedOrdersOnThisCounter = listOf<FoodOrder>()
    var placedCustomerOrders = listOf<Pair<Int, List<FoodOrder>>>()
    private var orderNo = 0

    fun viewMenu(): String {
        return Menu.values().map { item -> item.toString() }.toString()
    }

    fun placeCustomerFoodOrder(foodList: List<Pair<Menu, Int>>, bookingId: String): Int {
        val placedFoodList: List<FoodOrder> = foodList
            .mapIndexed { index, foodEntry -> FoodOrder(foodEntry.first, foodEntry.second, Start, bookingId, index) }
        val customerOrderId: Int = orderNo
        val customerOrder: Pair<Int, List<FoodOrder>> = (customerOrderId to placedFoodList)
        updateFoodCounterStatus(customerOrder)

        return customerOrderId
    }

    private fun updateFoodCounterStatus(customerOrder: Pair<Int, List<FoodOrder>>) {
        incrementOrderNumber()
        updatePlacedCustomerOrdersAtCounter(customerOrder)
    }

    private fun incrementOrderNumber() {
        orderNo += 1
    }

    private fun updatePlacedCustomerOrdersAtCounter(customerOrder: Pair<Int, List<FoodOrder>>) {
        placedCustomerOrders = placedCustomerOrders + customerOrder
    }

    fun viewOrder(orderId: Int): String {
        return placedCustomerOrders.filter { order: Pair<Int, List<FoodOrder>> -> order.first == orderId }
            .map { customerOrder: Pair<Int, List<FoodOrder>> ->
                customerOrder.second
                    .map { order: FoodOrder ->
                        order.foodDish.toString() + " " +
                                order.quantity.toString() + " " +
                                order.orderStatus.toString()
                    }
            }.toString()
    }

    fun calculateBill(bookingId: String): Int {
        val listOfFoodOrders = placedCustomerOrders.map { customerOrders -> customerOrders.second }
        return listOfFoodOrders.map { order: List<FoodOrder> -> order
                .fold(0) { acc: Int, ord: FoodOrder ->
                    if (ord.bookingId == bookingId) acc + ord.cost else 0 } }.sum()
    }
}