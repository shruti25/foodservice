import Status.InTheKitchen

class FoodOrder(val foodDish : Menu, val quantity : Int, val orderStatus : Status = InTheKitchen, val bookingId : String, val orderId : Int) {
    val cost = foodDish.cost * quantity
}