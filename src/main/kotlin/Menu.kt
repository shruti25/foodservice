enum class Menu(val cost : Int = 0) {
    SOUP(100), CHINESE(200), THAI(300), DESSERT(50)
}