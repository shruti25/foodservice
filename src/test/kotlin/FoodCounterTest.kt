import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class FoodCounterTest {
    @Test
    fun shouldPresentMenucard() {
        val foodCounter = FoodCounter()

        val actual = foodCounter.viewMenu()
        val expected = "[SOUP, CHINESE, THAI, DESSERT]"

        assertEquals(expected, actual)
    }

    @Test
    fun shouldOrderDish() {
        val foodCounter = FoodCounter()
        val orderId = foodCounter.placeCustomerFoodOrder(listOf((Menu.CHINESE to 1), (Menu.SOUP to 1)), "abc")

        assertEquals(0, orderId)

    }

    @Test
    fun shouldViewOrder() {
        val foodCounter = FoodCounter()

        val orderId = foodCounter.placeCustomerFoodOrder(listOf((Menu.CHINESE to 1), (Menu.SOUP to 1)), "abc")
        val actual = foodCounter.viewOrder(orderId)
        val expected = "[[CHINESE 1 Start, SOUP 1 Start]]"

        assertEquals(expected, actual)
    }

    @Test
    fun shouldCalculateBillForBookingId() {
        val foodCounter = FoodCounter()

        foodCounter.placeCustomerFoodOrder(listOf((Menu.CHINESE to 1), (Menu.SOUP to 1)), "abc")
        foodCounter.placeCustomerFoodOrder(listOf((Menu.DESSERT to 1)), "abc")

        val actual = foodCounter.calculateBill("abc")
        val expected = 350

        assertEquals(expected, actual)
    }
}